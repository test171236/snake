// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection : uint8 {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKE_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	TArray<ASnakeElementBase*> elements;
	EMovementDirection NextMoveDir, LastMoveDir;
	float ElementSize;
	FVector StartPos;

	// Sets default values for this actor's properties
	ASnakeBase();

	void AddSnakeElement(int elem_num = 1);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void Die();

	UFUNCTION()
	void OnOverlap(AActor* Interactor);

	UFUNCTION()
	void SetCollisionEnabled(ECollisionEnabled::Type Collision);

};
