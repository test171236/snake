// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeElementBase.h"

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	start_time = 0;
	move_dir = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();

	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
	AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, 
	bool bFromSweep, const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner) && type == ESnakeElementType::HEAD)
		SnakeOwner->OnOverlap(OtherActor);
}

void ASnakeElementBase::MoveElement(FVector new_loc, EMovementDirection new_move_dir)
{
	start_time = GetWorld()->GetTimeSeconds();
	cur_loc = new_loc;
	SetActorLocation(cur_loc);
	if (type != ESnakeElementType::HEAD)
		SetActorScale3D(FVector(1.));
	else
		SetActorScale3D(FVector(0.));

	move_dir = new_move_dir;
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (type == ESnakeElementType::BODY)
		return;

	FVector box = MeshComponent->GetStaticMesh()->GetBounds().GetBox().GetSize();
	float interval = SnakeOwner->GetActorTickInterval();
	float t = (GetWorld()->GetTimeSeconds() - start_time) / interval;
	t = (t > 1. ? 1. : t);
	t = (t < 0. ? 0. : t);

	FVector scale(1.);
	if (type == ESnakeElementType::HEAD)
		if (move_dir == EMovementDirection::UP || move_dir == EMovementDirection::DOWN)
			scale.X = t;
		else
			scale.Y = t;
	else
		if (move_dir == EMovementDirection::UP || move_dir == EMovementDirection::DOWN)
			scale.X = 1. - t;
		else
			scale.Y = 1. - t;
	SetActorScale3D(scale);
	
	FVector loc = cur_loc;
	if (move_dir == EMovementDirection::UP) {
		if (type == ESnakeElementType::HEAD)
			loc.X -= box.X / 2. * (1. - t);
		else
			loc.X += box.X / 2. * t;
		SetActorLocation(loc);
	} else if (move_dir == EMovementDirection::DOWN) {
		if (type == ESnakeElementType::HEAD)
			loc.X += box.X / 2. * (1. - t);
		else
			loc.X -= box.X / 2. * t;
		SetActorLocation(loc);
	} else if (move_dir == EMovementDirection::LEFT) {
		if (type == ESnakeElementType::HEAD)
			loc.Y -= box.Y / 2. * (1. - t);
		else
			loc.Y += box.Y / 2. * t;
		SetActorLocation(loc);
	} else {
		if (type == ESnakeElementType::HEAD)
			loc.Y += box.Y / 2. * (1. - t);
		else
			loc.Y -= box.Y / 2. * t;
		SetActorLocation(loc);
	}
	
}

void ASnakeElementBase::SetType_Implementation(ESnakeElementType new_type)
{
	type = new_type;
}

void ASnakeElementBase::Interact(AActor* Interactor)
{
	if (!IsValid(Interactor))
		return;

	ASnakeBase *snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(snake))
		snake->Die();
}
