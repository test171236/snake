// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeBase.h"
#include "SnakeElementBase.generated.h"

class ASnakeBase;

UENUM()
enum class ESnakeElementType : uint8 {
	HEAD,
	BODY,
	TAIL
};

UCLASS(Blueprintable)
class SNAKE_API ASnakeElementBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
	double start_time;
	FVector cur_loc;

public:
	EMovementDirection move_dir;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ESnakeElementType type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* MeshComponent;

	// Sets default values for this actor's properties
	ASnakeElementBase();

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY()
	ASnakeBase* SnakeOwner;

	UFUNCTION(BlueprintNativeEvent)
	void SetType(ESnakeElementType new_type);
	void SetType_Implementation(ESnakeElementType new_type);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor) override;

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
		AActor* OtherActor, UPrimitiveComponent* OtherComponent,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	void MoveElement(FVector new_loc, EMovementDirection new_move_dir);
};
