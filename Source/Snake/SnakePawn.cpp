// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakePawn.h"

#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Wall.h"
#include "Food.h"

// Sets default values
ASnakePawn::ASnakePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));

	RootComponent = PawnCamera;
}

void ASnakePawn::HandlerPlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDir != EMovementDirection::DOWN)
			SnakeActor->NextMoveDir = EMovementDirection::UP;
		if (value < 0 && SnakeActor->LastMoveDir != EMovementDirection::UP)
			SnakeActor->NextMoveDir = EMovementDirection::DOWN;
	}
}

void ASnakePawn::HandlerPlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value < 0 && SnakeActor->LastMoveDir != EMovementDirection::LEFT)
			SnakeActor->NextMoveDir = EMovementDirection::RIGHT;
		if (value > 0 && SnakeActor->LastMoveDir != EMovementDirection::RIGHT)
			SnakeActor->NextMoveDir = EMovementDirection::LEFT;
	}
}

// Called when the game starts or when spawned
void ASnakePawn::BeginPlay()
{
	Super::BeginPlay();

	SetActorRotation(FRotator(-90, 0, 0));
    LoadLevel1();
}

// Called every frame
void ASnakePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    if (IsValid(SnakeActor)) {
        FVector new_loc = ((AActor*)(SnakeActor->elements[0]))->GetActorLocation();
        new_loc.Z = 1000;
        FVector dir = new_loc - GetActorLocation();
        SetActorLocation(GetActorLocation() + dir * DeltaTime);
    }
}

// Called to bind functionality to input
void ASnakePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &ASnakePawn::HandlerPlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &ASnakePawn::HandlerPlayerHorizontalInput);
}

void ASnakePawn::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass);
}

void ASnakePawn::LoadLevel1(void)
{
    const FString lvl1 = R"(
                            #############################
                            ###      ####F   #          #
                            ### #    #       # F        #
                            #  F#    #       #######    #
                            # ###            #          #
                            #   ###F    #F           F  #
                            ### ###########   F  #      #
                            # F           # #### # #### #
                            #           #F#      #   F  #
                            #########   # #  F   #      #
                            #          ## ######## ###  #
                            #    ##### #             #  #
                            ###  #F  # #     F       #  #
                            #        # #####   ####  #  #
                            #   ##F  #    F#   #F    #  #
                            # #  ### #     #   #     #  #
                            # #F     # ##### # #   # #F #
                            # ###### # #F    # ##### ## #
                            #$   F   #       #   F      #
                            #############################)";
    UClass* BPClass = LoadClass<ASnakeElementBase>(nullptr, TEXT("Blueprint'/Game/SnakeElementBP.SnakeElementBP_C'"));
    float ElementSize = Cast<ASnakeElementBase>(BPClass->GetDefaultObject())->ElementSize;
    UClass* WallBPClass = LoadClass<AWall>(nullptr, TEXT("Blueprint'/Game/WallBP.WallBP_C'"));
    UClass* FoodBPClass = LoadClass<AFood>(nullptr, TEXT("Blueprint'/Game/FoodBP.FoodBP_C'"));
    FVector cur_p(0);

    for (uint8 c : lvl1)
        switch (static_cast<ELevelEntities>(c))
        {
        case ELevelEntities::Space:
            cur_p += FVector(0, ElementSize, 0);
            break;
        case ELevelEntities::Wall:
            GetWorld()->SpawnActor(WallBPClass, &cur_p);
            cur_p += FVector(0, ElementSize, 0);
            break;
        case ELevelEntities::Food:
            GetWorld()->SpawnActor(FoodBPClass, &cur_p);
            cur_p += FVector(0, ElementSize, 0);
            break;
        case ELevelEntities::NextRow:
            cur_p += FVector(-ElementSize, 0, 0);
            cur_p.Y = 0;
            break;
        case ELevelEntities::Start:
            SnakeActor = GetWorld()->SpawnActorDeferred<ASnakeBase>(SnakeActorClass, FTransform());
            SnakeActor->StartPos = cur_p;
            SnakeActor->FinishSpawning(FTransform());
            SetActorLocation(FVector(cur_p.X, cur_p.Y, 1000));

            cur_p += FVector(0, ElementSize, 0);
            break;
        default:
            break;
        }
}
