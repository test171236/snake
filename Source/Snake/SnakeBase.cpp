// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "Interactable.h"
#include "SnakeElementBase.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/GameEngine.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	NextMoveDir = EMovementDirection::UP;
	UClass* BPClass = LoadClass<ASnakeElementBase>(nullptr, TEXT("Blueprint'/Game/SnakeElementBP.SnakeElementBP_C'"));
	ElementSize = Cast<ASnakeElementBase>(BPClass->GetDefaultObject())->ElementSize;
}

void ASnakeBase::AddSnakeElement(int elem_num)
{
	if (elem_num <= 0)
		return;

	UClass* BPClass = LoadClass<ASnakeElementBase>(nullptr, TEXT("Blueprint'/Game/SnakeElementBP.SnakeElementBP_C'"));
	
	int start = elements.Num();
	if (start >= 2)
		elements[start - 1]->SetType(ESnakeElementType::BODY);

	for (int i = 0; i < elem_num; i++)
	{
		FVector loc(-(start + i) * ElementSize, 0, 20000);

		//������ ���� �������� ��� ��������� �������� ����� ������ ���-������ �����
		if (elements.Num() == 0)
			loc = StartPos;

		FTransform trans(loc);
		
		ASnakeElementBase* new_el = GetWorld()->SpawnActor<ASnakeElementBase>(BPClass, trans);
		new_el->SnakeOwner = this;

		if (elements.Num() == 0)
			new_el->SetType(ESnakeElementType::HEAD);
		else if (i < elem_num - 1)
			new_el->SetType(ESnakeElementType::BODY);
		else
			new_el->SetType(ESnakeElementType::TAIL);
		
		elements.Add(new_el);
	}	
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(1);

	const int start_elem_num = 5;

	ASnakeBase::AddSnakeElement(start_elem_num);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector move_vec(0, 0, 0);
	float move_speed_delta = ElementSize;

	switch (NextMoveDir)
	{
	case EMovementDirection::UP:
		move_vec.X += move_speed_delta;
		break;
	case EMovementDirection::DOWN:
		move_vec.X -= move_speed_delta;
		break;
	case EMovementDirection::LEFT:
		move_vec.Y += move_speed_delta;
		break;
	case EMovementDirection::RIGHT:
		move_vec.Y -= move_speed_delta;
		break;
	}

	SetCollisionEnabled(ECollisionEnabled::NoCollision);

	FVector prev_loc;
	EMovementDirection prevprev_dir, prev_dir, cur_dir;
	int i = 0;

	//Head
	FVector new_loc = (elements[i]->GetActorLocation() + move_vec);
	prev_loc = elements[i]->GetActorLocation();
	cur_dir = elements[i]->move_dir;
	elements[i]->MoveElement(new_loc, NextMoveDir);
	prev_dir = cur_dir;

	//Body
	for (i = 1; i < elements.Num() - 1; i++)
	{
		new_loc = prev_loc;
		prev_loc = elements[i]->GetActorLocation();
		cur_dir = elements[i]->move_dir;
		elements[i]->MoveElement(new_loc, prev_dir);
		prevprev_dir = prev_dir;
		prev_dir = cur_dir;
	}

	//Tail
	new_loc = prev_loc;
	prev_loc = elements[i]->GetActorLocation();
	cur_dir = elements[i]->move_dir;
	elements[i]->MoveElement(new_loc, prevprev_dir);

	SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	LastMoveDir = NextMoveDir;
}

void ASnakeBase::Die()
{
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Red, TEXT("Game over!"));
	this->Destroy();
}

void ASnakeBase::OnOverlap(AActor* Interactor)
{
	if (!IsValid(Interactor))
		return;

	IInteractable* inter = Cast<IInteractable>(Interactor);
	if (inter)
		inter->Interact(this);
}

void ASnakeBase::SetCollisionEnabled(ECollisionEnabled::Type Collision)
{
	for (int i = 0; i < elements.Num(); i++)
		elements[i]->MeshComponent->SetCollisionEnabled(Collision);
}
