// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SnakePawn.generated.h"

class UCameraComponent;
class ASnakeBase;

UENUM()
enum class ELevelEntities : uint8 {
	Space = ' ',
	Wall = '#',
	Start = '$',
	Food = 'F',
	NextRow = '\n',
	End = '\0'
};

UCLASS()
class SNAKE_API ASnakePawn : public APawn
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite);
	UCameraComponent* PawnCamera;
	
	UPROPERTY(BlueprintReadWrite);
	ASnakeBase* SnakeActor;
	
	UPROPERTY(EditDefaultsOnly);
	TSubclassOf<ASnakeBase> SnakeActorClass;

	// Sets default values for this pawn's properties
	ASnakePawn();

	UFUNCTION()
	void HandlerPlayerVerticalInput(float value);
	UFUNCTION()
	void HandlerPlayerHorizontalInput(float value);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	void LoadLevel1(void);
};
